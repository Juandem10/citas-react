# Control de citas

Proyecto de control de citas realizado con react por medio de vite.

## Instalación
La versión de node utilizada es la 16.13.2.
Clonar el repositorio, ubicarte en la carpeta y ejecutar el siguiente comando.

```bash
npm install
```

## Usage

```javascript
npm run dev
```

## License
[MIT](https://choosealicense.com/licenses/mit/)